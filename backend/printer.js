function printMessage (date, city, country, temp, textcond, cond, press, humidity, feelslike) {
  console.log('Bonjour, ');
  console.log(date);
  console.log('A ' + city + ', ' + country + ' la température est de ' + temp + ' °C, la pression atmosphérique est de ' + press + ' hPa.');
  console.log('Les conditions sont ' + textcond + ', le ressentit est de ' + feelslike + ' °C et le taux d\'humidité dans l\'air est de ' + humidity + '%');
}

function printError (error){
  console.error(error.message);
}

module.exports.printMessage = printMessage;
module.exports.printError = printError;
