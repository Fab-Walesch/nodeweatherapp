const http = require('http');
const printer = require('./printer.js');

let url = 'http://api.apixu.com/v1/current.json?key=';
let apiKey = '19e8726c4a2841f2900100146180507';
let city;

function getData (city) {
  let request = http.get( url + apiKey + '&lang=fr&q=' + city, (response) => {
    let body ='';
    response.on('data', (chunck) => {
      body += chunck;
    });

    response.on('end', () => {
      if(response.statusCode === 200){
        try {
          let dataWeather = JSON.parse(body);
          let date = dataWeather.location.localtime;
          let city = dataWeather.location.name;
          let country = dataWeather.location.country;
          let temp = dataWeather.current.temp_c;
          let textcond = dataWeather.current.condition.text;
          let cond = dataWeather.current.condition.icon;
          let press = dataWeather.current.pressure_mb;
          let humidity = dataWeather.current.humidity;
          let feelslike = dataWeather.current.feelslike_c;

          printer.printMessage (date, city, country, temp, textcond, cond, press, humidity, feelslike);
        }catch(error){
          printer.printError(error);
        }
      }else{
          printer.printError({ message : 'Impossible de récupérer les informations !!!' });
      }
    });
  });
}

module.exports.getData = getData;
